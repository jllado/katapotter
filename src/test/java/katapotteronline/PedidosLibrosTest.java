package katapotteronline;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PedidosLibrosTest {

    private PedidoLibros pedidoLibros;

    @Before
    public void setUp() {
        pedidoLibros = new PedidoLibros();
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_un_libro() {
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO1));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().get(0), is(new Libro(Libro.LIBRO1)));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_un_libro_con_diferente_titulo() {
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO2));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().get(0), is(new Libro(Libro.LIBRO2)));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_dos_libros() {
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO1));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().get(0), is(new Libro(Libro.LIBRO1)));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().get(0), is(new Libro(Libro.LIBRO1)));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_dos_libros_diferentes() {
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO2));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(2));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_dos_libros_iguales_y_uno_diferente() {
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO2));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(2));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(3));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(1).libros().size(), is(1));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_dos_libros_iguales_y_dos_diferente() {
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO2));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(2));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(4));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(3).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(1).libros().size(), is(2));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_tres_libros_iguales_y_dos_diferente() {
        pedidoLibros.anyadir(3, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO2));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(2));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(5));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(3).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(4).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(3));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(1).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(2).libros().size(), is(1));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_tres_libros_diferente() {
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO2));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO3));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(3));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(3));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(2).getPaquetes().size(), is(1));
        assertThat(gruposLibros.get(2).getPaquetes().get(0).libros().size(), is(3));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_2_1_1() {
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO2));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO3));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(3));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(4));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(3).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(1).libros().size(), is(2));
        assertThat(gruposLibros.get(2).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(2).getPaquetes().get(0).libros().size(), is(3));
        assertThat(gruposLibros.get(2).getPaquetes().get(1).libros().size(), is(1));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_3_1_1() {
        pedidoLibros.anyadir(3, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO2));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO3));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(3));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(5));
        assertThat(gruposLibros.get(0).getPaquetes().get(0).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(3).libros().size(), is(1));
        assertThat(gruposLibros.get(0).getPaquetes().get(4).libros().size(), is(1));
        assertThat(gruposLibros.get(1).getPaquetes().size(), is(3));
        assertThat(gruposLibros.get(1).getPaquetes().get(0).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(1).libros().size(), is(2));
        assertThat(gruposLibros.get(1).getPaquetes().get(2).libros().size(), is(1));
        assertThat(gruposLibros.get(2).getPaquetes().size(), is(3));
        assertThat(gruposLibros.get(2).getPaquetes().get(0).libros().size(), is(3));
        assertThat(gruposLibros.get(2).getPaquetes().get(1).libros().size(), is(1));
        assertThat(gruposLibros.get(2).getPaquetes().get(2).libros().size(), is(1));
    }

    @Test
    public void obtener_lista_grupos_para_un_pedido_2_2_2_1_1() {
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO1));
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO3));
        pedidoLibros.anyadir(2, new Libro(Libro.LIBRO2));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO4));
        pedidoLibros.anyadir(1, new Libro(Libro.LIBRO5));

        List<GrupoLibros> gruposLibros = pedidoLibros.getGrupos();

        assertThat(gruposLibros.size(), is(5));
        assertThat(gruposLibros.get(0).getPaquetes().size(), is(8));
        assertThat(gruposLibros.get(3).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(3).getPaquetes().get(0).libros().size(), is(4));
        assertThat(gruposLibros.get(3).getPaquetes().get(1).libros().size(), is(4));
        assertThat(gruposLibros.get(4).getPaquetes().size(), is(2));
        assertThat(gruposLibros.get(4).getPaquetes().get(0).libros().size(), is(5));
        assertThat(gruposLibros.get(4).getPaquetes().get(1).libros().size(), is(3));
    }

}
