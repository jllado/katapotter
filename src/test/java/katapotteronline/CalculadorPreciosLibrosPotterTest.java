package katapotteronline;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class CalculadorPreciosLibrosPotterTest {

    private CalculadorPreciosLibrosPotter calculadora;
    private PedidoLibros pedido;

    @Before
    public void setUp() {
        pedido = new PedidoLibros();
        calculadora = new CalculadorPreciosLibrosPotter();
    }

    @Test
    public void calcular_precio_un_libro() {
        pedido.anyadir(1, new Libro("libro 1"));

        assertThat(cacularPrecioDe(pedido), is(8f));
    }

    @Test
    public void calcular_precio_dos_libros_iguales() {
        pedido.anyadir(2, new Libro("libro 1"));

        assertThat(cacularPrecioDe(pedido), is(16f));
    }

    @Test
    public void calcular_precio_dos_libros_distintos() {
        pedido.anyadir(1, new Libro("libro 1"));
        pedido.anyadir(1, new Libro("libro 2"));

        assertThat(cacularPrecioDe(pedido), is(15.2f));
    }

    @Test
    public void calcular_precio_tres_libros_distintos() {
        pedido.anyadir(1, new Libro("libro 1"));
        pedido.anyadir(1, new Libro("libro 2"));
        pedido.anyadir(1, new Libro("libro 3"));

        assertThat(cacularPrecioDe(pedido), is(21.6f));
    }

    @Test
    public void calcular_precio_dos_iguales_y_uno_distinto() {
        pedido.anyadir(2, new Libro("libro 1"));
        pedido.anyadir(1, new Libro("libro 2"));

        assertThat(cacularPrecioDe(pedido), is(23.2f));
    }

    @Test
    public void calcular_precio_2_2_2_1_1() {
        pedido.anyadir(2, new Libro("libro 1"));
        pedido.anyadir(2, new Libro("libro 2"));
        pedido.anyadir(2, new Libro("libro 3"));
        pedido.anyadir(1, new Libro("libro 4"));
        pedido.anyadir(1, new Libro("libro 5"));

        assertThat(cacularPrecioDe(pedido), is(51.20f));
    }

    private float cacularPrecioDe(PedidoLibros pedido) {
        return calculadora.calcularPrecioDe(pedido);
    }
}
