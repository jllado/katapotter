package katapotteronline;

import java.util.ArrayList;
import java.util.List;

public class GrupoLibros {

    private List<PaqueteLibros> paquetes;

    public GrupoLibros() {
        paquetes = new ArrayList<PaqueteLibros>();
    }

    public void anyadir(PaqueteLibros paquete) {
        paquetes.add(paquete);
    }

    public List<PaqueteLibros> getPaquetes() {
        return paquetes;
    }

    public int tamanyo() {
        return paquetes.size();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GrupoLibros other = (GrupoLibros) obj;
        if (paquetes == null) {
            if (other.paquetes != null)
                return false;
        } else if (!paquetes.equals(other.paquetes))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GrupoCopiasLibros [paquetes=" + paquetes + "]";
    }

}
