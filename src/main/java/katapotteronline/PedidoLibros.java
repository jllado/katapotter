package katapotteronline;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class PedidoLibros {

    private Map<Libro, Integer> libros;

    public PedidoLibros() {
        libros = new LinkedHashMap<Libro, Integer>();
    }

    public void anyadir(int cantidad, Libro libro) {
        libros.put(libro, cantidad);
    }

    public int cantidad() {
        return libros.size();
    }

    public Set<Entry<Libro, Integer>> getCopiasLibros() {
        return libros.entrySet();
    }

    public Map<Libro, Integer> libros() {
        return libros;
    }

    public List<GrupoLibros> getGrupos() {
        List<GrupoLibros> grupos = new ArrayList<GrupoLibros>();
        for (int tamanyoPaquete = 1; tamanyoPaquete <= libros.size(); tamanyoPaquete++) {
            grupos.add(getGrupoCon(tamanyoPaquete));
        }

        return grupos ;
    }

    private GrupoLibros getGrupoCon(int tamanyoPaquete) {
        GrupoLibros grupo = new GrupoLibros();

        ContadorLibros contadorLibros = new ContadorLibros(libros);
        while (quedanLibrosDel(contadorLibros)) {
            grupo.anyadir(
                    getPaqueteCon(
                            tamanyoPaquete(tamanyoPaquete, contadorLibros),
                            contadorLibros));
            contadorLibros.quitarLibrosVacios();
        }

        return grupo;
    }

    private PaqueteLibros getPaqueteCon(int tamanyoPaquete, ContadorLibros contadorLibros) {
        PaqueteLibros paquete = new PaqueteLibros();
        for (Libro libro : contadorLibros.libros()) {
            if (alcanzado(tamanyoPaquete, paquete)) {
                return paquete;
            }
            if (paquete.noTiene(libro)) {
                paquete.anyadir(libro);
                contadorLibros.restarUn(libro);
            }
        }
        return paquete;
    }

    private int tamanyoPaquete(
            int tamanyoPaquete,
            ContadorLibros contadorLibros) {
        if (contadorLibros.numeroLibros() >= tamanyoPaquete) {
            return tamanyoPaquete;
        } else {
            return contadorLibros.numeroLibros();
        }
    }

    private boolean quedanLibrosDel(ContadorLibros contadorLibros) {
        return contadorLibros.totalLibros()  > 0;
    }

    private boolean alcanzado(int tamanyoPaquete, PaqueteLibros paquete) {
        return paquete.tamanyo() == tamanyoPaquete;
    }
}