package katapotteronline;

import java.util.List;

public class CalculadorPreciosLibrosPotter {

    private static final float PRECIO_LIBRO = 8f;
    private float[] descuentos = {0, 1f, 0.95f, 0.9f, 0.8f, 0.75f};

    public float calcularPrecioDe(PedidoLibros pedido) {
        return redondear(getMejorPrecioDe(pedido.getGrupos()));
    }

    private float getMejorPrecioDe(List<GrupoLibros> grupos) {
        float precio = 0;
        for (GrupoLibros grupo : grupos) {
            float precioGrupo = getPrecioDe(grupo);
            if (precio == 0 || precioGrupo < precio) {
                precio = precioGrupo;
            }
        }
        return precio;
    }

    private float getPrecioDe(GrupoLibros grupoLibros) {
        float precio = 0;
        for (PaqueteLibros paquete : grupoLibros.getPaquetes()) {
            precio += PRECIO_LIBRO * paquete.tamanyo() * descuentos[paquete.tamanyo()];
        }
        return precio;
    }

    private float redondear(float precio) {
        return Math.round(precio * 100.0f) / 100.0f;
    }
}
