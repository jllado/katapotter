package katapotteronline;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class ContadorLibros {

    private Map<Libro, Integer> libros;
    private int totalLibros = 0;
    private Set<Libro> librosVacios;

    public ContadorLibros() {
        librosVacios = new HashSet<Libro>();
    }

    public ContadorLibros(Map<Libro, Integer> libros) {
        librosVacios = new HashSet<Libro>();
        this.libros = crearContadorDe(libros);
    }

    public void restarUn(Libro libro) {
        totalLibros--;
        libros.put(libro, libros.get(libro) - 1);
        if (libros.get(libro) == 0) {
            librosVacios.add(libro);
        }
    }

    public int getCantidadDe(Libro libro) {
        return libros.get(libro);
    }

    public int totalLibros() {
        return totalLibros;
    }

    public Set<Libro> libros() {
        return libros.keySet();
    }

    private Map<Libro, Integer> crearContadorDe(Map<Libro, Integer> libros) {
        totalLibros = 0;
        for (Entry<Libro, Integer> infoLibro : libros.entrySet()) {
            totalLibros += infoLibro.getValue();
        }
        return new LinkedHashMap<Libro, Integer>(libros);
    }

    public int numeroLibros() {
        return libros.size();
    }

    public void quitarLibrosVacios() {
        for (Libro libro : librosVacios) {
            libros.remove(libro);
        }
        librosVacios.clear();
    }
}