package katapotteronline;

import java.util.ArrayList;
import java.util.List;

public class PaqueteLibros {

    private List<Libro> libros;

    public PaqueteLibros() {
        libros = new ArrayList<Libro>();
    }

    public void anyadir(Libro libro) {
        libros.add(libro);
    }

    public int tamanyo() {
        return libros.size();
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PaqueteCopiasLibros [libros=" + libros + "]";
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PaqueteLibros other = (PaqueteLibros) obj;
        if (libros == null) {
            if (other.libros != null)
                return false;
        } else if (!libros.equals(other.libros))
            return false;
        return true;
    }

    public List<Libro> libros() {
        return libros;
    }

    public boolean noTiene(Libro libro) {
        return !libros.contains(libro);
    }

}