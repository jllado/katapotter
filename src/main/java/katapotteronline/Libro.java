package katapotteronline;

public class Libro {
    public static final String LIBRO1 = "libro 1";
    public static final String LIBRO2 = "libro 2";
    public static final String LIBRO3 = "libro 3";
    public static final String LIBRO4 = "libro 4";
    public static final String LIBRO5 = "libro 5";

    private String titulo = "";

    public Libro(String pTitulo) {
        titulo = pTitulo;
    }

    /**
     * @return the nombre
     */
    public final String getTitulo() {
        return titulo;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Libro other = (Libro) obj;
        if (titulo == null) {
            if (other.titulo != null)
                return false;
        } else if (!titulo.equals(other.titulo))
            return false;
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Libro [titulo=" + titulo + "]";
    }

}
